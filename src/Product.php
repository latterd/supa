<?php

namespace Supa;

/**
 * Product class
 */
class Product {
	
	protected $id;
	protected $active;
	protected $title;
	protected $description;
	protected $categories;
	protected $attributes;
	protected $images;
	protected $price;
	protected $featured;
	protected $lastEditied;
	protected $created;
	protected $lastEditiedBy;
	protected $createdBy;

	/**
	 * Constructor for product class
	 *
	 * @param array $data Product data
	 */
	public function __construct($data) {
		$this->assignClassVariables($data);
	}
	
	/**
	 * Determine if product is active
	 *
	 * @return bool True if product active, false otherwise
	 */
	public function isActive() {
		return (bool)$this->active;
	}
	
	/**
	 * Determine if product is featured
	 * 
	 * @return bool True if product 'featured', false otherwise
	 */
	public function isFeatured() {
	    return (bool)$this->featured;
	}
	
	/**
	 * Get the product title
	 * 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Get the product id
	 * 
	 * @return int
	 */
	public function getID() {
		return $this->id;
	}
	
	/**
	 * Set the product id
	 * 
	 * @param int $id
	 */
	public function setID($id) {
		$this->id = (int)$id;
	}
	
	/**
	 * Get the product path
	 * 
	 * @return string 
	 */	
	public function getPath() {
		return $this->path;
	}
	
	/**
	 * Get the product description
	 * 
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * Set the product description
	 * 
	 * @param string $desc
	 */
	public function setDescription($desc) {
		$this->description = $desc;
	}
	
	/**
	 * Set the product title
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Set the active state for the product
	 * 1 = active, 0 = not active
	 * 
	 * @param int $state A 1 (active) or 0 (not-active)
	 */
	public function setActiveState($state) {
		$this->active = (int)$state;
	}
	
	/**
	 * Get the timestamp of when this product was created
	 * 
	 * @return string
	 */	
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * Set the timestamp of when this product was created
	 * 
	 * @param string $strFormattedTime
	 */	
	public function setCreated($strFormattedTime) {
		$this->created = $strFormattedTime;
	}

	/**
	 * Get the user who created product
	 * 
	 * @return User
	 */	
	public function getCreatedBy() {
		return $this->createdBy;
	}
	
	/**
	 * Set the user who created product
	 * 
	 * @param $user
	 */	
	public function setCreatedBy($user) {
		$this->createdBy = $user;
	}

	/**
	 * Get timestamp of when product user was last editied
	 * 
	 * @return string|null
	 */
	public function getLastEditied() {
		return $this->lastEditied;
	}

	/**
	 * Set timestamp of when this product was last editied
	 * 
	 * @param string $lastEditiedTime
	 */
	public function setLastEditied($lastEditiedTime) {
		$this->lastEditied = $lastEditiedTime;
	}

	/**
	 * Set product attributes for this product
	 * 
	 * @param array $attributes
	 */
	public function setAttributes($attributes) {
	    $this->attributes = $attributes;
	}

	/**
	 * Get all product attributes
	 * 
	 * @return array
	 */
	public function getAttributes() {
		return $this->attributes;
	}
	
	/**
	 * Get the product's categories
	 * 
	 * @return array
	 */
	public function getCategories() {
		return $this->categories;
	}
	
	/**
	 * Set the product's categories
	 * 
	 * @param array $categories
	 */
	public function setCategories($categories) {
		$this->categories = $categories;
	}
	
	/**
	 * Set default price
	 * 
	 * @param string|int|float $price
	 */
	public function setPrice($price) {
	    $this->price = (float)$price;
	}
	
	/**
	 * Get default price
	 * 
	 * @return float Default price for product
	 */
	public function getPrice() {
	    return (float)$this->price;
	}
	
	/**
	 * Get product images
	 * 
	 * @return array
	 */
	public function getImages() {
		return $this->images;
	}
	
	public function setFeatured($bool) {
	    $this->featured = $bool;
	}
	
	/**
	 * Set images for the product
	 * 
	 * @param array $images
	 */
	public function setImages($images) {
		$this->images = $images;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
	
	public function toArray() {
	    return get_object_vars($this);
	}
}