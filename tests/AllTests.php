<?php

require_once('/var/www/supa/vendor/autoload.php');
require_once('Product/ProductTestSuite.php');
require_once('Product/ProductTest.php');
require_once('User/UserTestSuite.php');
require_once('User/UserTest.php');

/**
 * Static test suite.
 */
class AllTests extends \PHPUnit_Framework_TestSuite {

	static $rootPath = null;

	public function __construct(){
		$this->setName('TestSuite');
        $this->addTestSuite('ProductTestSuite');
        $this->addTestSuite('UserTestSuite');
	}

	public static function suite(){
		return new self();
	}
}
